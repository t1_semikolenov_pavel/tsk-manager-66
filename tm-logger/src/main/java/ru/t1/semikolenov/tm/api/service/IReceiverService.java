package ru.t1.semikolenov.tm.api.service;

import org.jetbrains.annotations.NotNull;

import javax.jms.MessageListener;

public interface IReceiverService {

    void receive(@NotNull MessageListener listener);

}
