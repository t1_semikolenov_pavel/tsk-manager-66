package ru.t1.semikolenov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1.semikolenov.tm.dto.model.UserDTO;

@Repository
public interface UserDtoRepository extends AbstractDtoRepository<UserDTO> {

    @Nullable
    UserDTO findFirstByLogin(@NotNull String login);

    @Nullable
    UserDTO findFirstByEmail(@NotNull String email);

}
