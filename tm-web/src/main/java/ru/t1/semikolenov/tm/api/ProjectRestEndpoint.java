package ru.t1.semikolenov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.t1.semikolenov.tm.model.Project;

import java.util.List;

@RequestMapping("/api/projects")
public interface ProjectRestEndpoint {

    @GetMapping("/findAll")
    List<Project> findAll();

    @GetMapping("/findById/{id}")
    Project findById(@NotNull @PathVariable("id") String id);

    @GetMapping("/existsById/{id}")
    boolean existsById(@NotNull @PathVariable("id") String id);

    @GetMapping("/count")
    long count();

    @PostMapping("/save")
    Project save(@NotNull @RequestBody Project project);

    @PostMapping("/delete")
    void delete(@NotNull @RequestBody Project project);

    @PostMapping("/deleteById/{id}")
    void deleteById(@NotNull @PathVariable("id") String id);

    @PostMapping("/deleteAll")
    void clear(@NotNull @RequestBody List<Project> projects);

    @PostMapping("/clear")
    void clear();

}
