package ru.t1.semikolenov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.semikolenov.tm.model.Project;

@Repository
public interface ProjectRepository extends JpaRepository<Project, String> {

}
